// https://github.com/fridays/next-routes
const routes = require("next-routes");

module.exports = routes()
  // .add({ name: "beta", pattern: "/v3", page: "v3" })
  .add("users", "/users/:nickname", "users/[nickname]") // user   profile   /user/:id
  // .add("users", "/users", "/users/:id")
  .add("flashes", "/flashes/:id/:slug")
  .add("flashetest", "/flashetest/:id/:slug")
  .add({
    name: "flashcards",
    pattern: "/flashcards/:id/:slug",
    page: "flashcards"
  })
  .add({
    name: "flashsetCreate",
    pattern: "/flashsets/create",
    page: "flashsets/create"
  })
  .add({
    name: "flashsetEdit",
    pattern: "/flashsets/:id/edit",
    page: "flashsets/[id]/edit"
  });

// Name   Page      Pattern
//   .add("user", "/user/:id", "profile") // user   profile   /user/:id
//   .add("/:noname/:lang(en|es)/:wow+", "complex") // (none) complex   /:noname/:lang(en|es)/:wow+
//   .add({ name: "beta", pattern: "/v3", page: "v3" }); // beta   v3        /v3
