import React, { Component } from "react";
import Item from "./item";
import uuid from "uuid";
import { saveFlashcardsUrl, deleteFlashcardsUrl } from "../../serverRoutes";
import axios from "axios";
// import Router from "../../routes";
import Router from "next/router";

class List extends Component {
  constructor(props) {
    super(props);
    const initList = [
      { uuid: uuid.v4(), term: "", definition: "" },
      { uuid: uuid.v4(), term: "", definition: "" }
    ];
    const list =
      props.flashcards && props.flashcards.length > 0
        ? [...props.flashcards, ...initList]
        : initList;

    this.state = {
      regex_cards: "",
      isLoading: !!this.props.flashsetId,
      flashset: {
        title: "",
        ...props.flashset
      },
      list: list.map(flash => ({ ...flash, uuid: uuid.v4() }))
    };
  }

  addInput = index => {
    let list = [...this.state.list];
    list.splice(index + 1, 0, { uuid: uuid.v4(), term: "", definition: "" });
    this.setState({ list });
  };
  saveFlashcards = () => {
    axios({
      method: saveFlashcardsUrl().method,
      url: saveFlashcardsUrl().path,
      data: {
        flashsetId: this.state.flashsetId,
        flashset: this.state.flashset,
        flashcards: this.state.list.filter(
          item => item.definition.length && item.term.length
        )
      }
    }).then(res => {
      if (
        !(
          window.location.href.includes("edit") &&
          window.location.href.includes("ediflashsetst")
        )
      ) {
        Router.push(`/flashsets/${res.data.flashset._id}/edit`);
      }
      this.setState({
        flashset: res.data.flashset,
        list: res.data.flashcards
      });
    });
  };
  removeItemFromList = ({ uuid }) => {
    this.setState({
      list: this.state.list.filter(_item => _item.uuid !== uuid)
    });
  };

  handledeleteItem = item => {
    if (!item._id) {
      this.removeItemFromList(item);
      return;
    }
    const url = deleteFlashcardsUrl({ id: item._id });
    axios({
      method: url.method,
      url: url.path
    })
      .then(this.removeItemFromList(item))
      .catch(err => console.log(err));
  };

  handleFlashcardChange = ({ name, value, itemUuid }) => {
    this.setState({
      list: this.state.list.map(item => {
        if (item.uuid == itemUuid) {
          return { ...item, [name]: value };
        }
        return item;
      })
    });
  };
  handleFlashsetNameChange = e => {
    this.setState({
      flashset: {
        ...this.state.flashset,
        title: event.target.value
      }
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  parseFlashcards = e => {
    let myRegEx = /(.+)\|-\|(.+)\n\n/g;
    let inputString = this.state.regex_cards;
    if (inputString && inputString.length > 0) {
      let newFlashcards = [];
      let matches = [...inputString.matchAll(myRegEx)];
      matches.map(match => {
        if (match[1] && match[2]) {
          newFlashcards.push({
            uuid: uuid.v4(),
            term: match[1].trim(),
            definition: match[2].trim()
          });
        }
      });
      this.setState({
        list: [
          ...this.state.list.filter(l => l.term.length || l.definition.length),
          ...newFlashcards
        ]
      });
    }
  };

  render() {
    return (
      <div>
        <div className="my-5 flashset__create">
          <div className="h3  mb-3 font-weight-bold">
            ფლეშქარდების ჯგუფის სათაური
          </div>
          <div className="row">
            <div className="col-md-7 mb-3">
              <div className="input__group w">
                <input
                  id="flashset"
                  name="flashset"
                  type="text"
                  placeholder="შეიყვანეთ ფლეშქარდების ჯგუფის სათაური"
                  className="material"
                  onChange={this.handleFlashsetNameChange}
                  value={this.state.flashset.title}
                />
                <div className="bar" />
              </div>
            </div>
            <div className="col-md-5 mb-3 pt-1">
              <button
                className="btn btn-info px-5"
                onClick={this.saveFlashcards}
              >
                შენახვა
              </button>
              {this.props.flashset && (
                <a
                  target="_blank"
                  href={`/flashes/${this.props.flashset._id}/${this.props.flashset.slug}`}
                  className="btn btn-warning text-white ml-2"
                >
                  ფლეშქარდების ნახვა
                </a>
              )}
            </div>
          </div>
        </div>
        <div className="plus_button_parent text-center pt-3">
          <button
            onClick={() => this.addInput(-1)}
            className="addButton__flashcard btn btn-warning"
          >
            <i className="fas fa-plus" />
          </button>
        </div>
        {this.state.list.map((item, index) => (
          <React.Fragment key={item._id ? item._id : item.uuid}>
            <Item
              key={item._id ? item._id : item.uuid}
              index={index}
              item={item}
              deleteItem={this.handledeleteItem}
              handleChange={this.handleFlashcardChange}
            />
            <div className="plus_button_parent text-center pt-3">
              <button
                onClick={() => this.addInput(index)}
                className="addButton__flashcard btn btn-warning"
              >
                <i className="fas fa-plus" />
              </button>
            </div>
          </React.Fragment>
        ))}
        <div>
          <textarea
            type="text"
            name="regex_cards"
            onChange={this.handleChange}
          />
          <button onClick={this.parseFlashcards}>Parse Regex</button>
        </div>
        <button
          className="btn btn-block btn-info"
          onClick={this.saveFlashcards}
        >
          შენახვა
        </button>
      </div>
    );
  }
}

export default List;
