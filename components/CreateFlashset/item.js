import React, { Component, PureComponent } from "react";
import PropTypes from "prop-types";
import InputGroup from "../../components/comon/InputGroup";

export class Item extends PureComponent {
  state = {
    deleteBtn: false,
    deleting: false
  };

  showDeleteButton = () => {
    this.setState({
      deleteBtn: true
    });
  };
  hideDeleteButton = () => {
    this.setState({
      deleteBtn: false
    });
  };
  onChange = e => {
    this.props.handleChange({
      name: e.target.name,
      value: e.target.value,
      itemUuid: this.props.item.uuid
    });
  };

  deleteItem = e => {
    e.preventDefault();
    if (!confirm("ნამდვილად გსურთ წაშლა")) return;
    this.setState({ deleting: true });
    this.props.deleteItem(this.props.item);
  };

  render() {
    return (
      <div className="b__shadow1 flashcard__create__container">
        <div
          className="fl__c__actions"
          onMouseEnter={this.showDeleteButton}
          onMouseLeave={this.hideDeleteButton}
        >
          {this.state.deleteBtn ? (
            <button onClick={this.deleteItem} className="btn btn-sm btn-danger">
              {this.state.deleting ? (
                <i className="fas fa-spinner fa-spin" />
              ) : (
                <i className="fa fa-trash" />
              )}
            </button>
          ) : (
            this.props.index + 1
          )}
        </div>
        <div className="term__i__container mx-3">
          <InputGroup
            onChange={this.onChange}
            value={this.props.item.term}
            placeholder="კითხვა"
            name="term"
            inputClasses={["material"]}
            type="text"
          />
          <div className="bar" />
        </div>
        <div className="definition__i__container mx-3">
          <InputGroup
            onChange={this.onChange}
            value={this.props.item.definition}
            placeholder="პასუხი"
            name="definition"
            inputClasses={["material"]}
            type="text"
          />
          <div className="bar" />
        </div>
      </div>
    );
  }
}

export default Item;

Item.PropTypes = {
  item: PropTypes.object.isRequired
};
