import React from "react";
import PropTypes from "prop-types";
import { Link } from "../../routes";

export default props => {
  const styleD =
    props.width && props.height
      ? { width: props.width, height: props.height }
      : null;
  return (
    <div className="some" style={{ perspective: "300" }}>
      <div id="logo__wrapper" style={styleD} className="layerslider-logo">
        <div className="logo__flash">
          <div />
        </div>
        <div className="logo__flash">
          <div />
        </div>
        <div className="logo__flash">
          <div />
        </div>
      </div>
    </div>
  );
};

() => {
  return (
    <div
      id="layerslider-logo"
      className="layerslider-logo-animated layerslider-logo"
    >
      <div className="layerslider-logo-setheight" />
      <div className="layerslider-logo-layer">
        <div className="layerslider-logo-inner" />
      </div>
      <div className="layerslider-logo-layer">
        <div className="layerslider-logo-inner" />
      </div>
      <div className="layerslider-logo-layer">
        <div className="layerslider-logo-inner" />
      </div>
    </div>
  );
};
