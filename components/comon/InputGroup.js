import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

const InputGroup = ({
  name,
  type,
  placeholder,
  value,
  error,
  onChange,
  inputClasses
}) => {
  return (
    <React.Fragment>
      {/* <span> */}
      <textarea
        onChange={onChange}
        id={name}
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        autoComplete="off"
        className={classnames("form", [...inputClasses])}
      />
      <span className="underline" />
      {error && <p className="text-danger mt-m-20">{error.msg}</p>}
      {/* </span> */}
    </React.Fragment>
  );
};

InputGroup.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  error: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  inputClasses: PropTypes.array
};

InputGroup.defaultProps = {
  inputClasses: [],
  type: "text"
};

export default InputGroup;
