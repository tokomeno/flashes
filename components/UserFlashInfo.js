import React from "react";

export default function UserFlashInfo() {
  return (
    <section>
      <div className="user__flash__info">
        <div className="user__info">
          <div className="user__avatar">
            <img src="https://gimg.quizlet.com/-_Uv01hFT798/AAAAAAAAAAI/AAAAAAAAAjM/-kDnfadKBtA/photo.jpg?sz=24" />
          </div>
          <a href="" className="href__n __info d-flex flex-column py-2 ">
            <div className="creator">ავტორი</div>
            <div className="nickname bold">tokomeno</div>
          </a>
        </div>
      </div>
      {/* <p className="flash__desc">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Natus dolore
          reprehenderit facilis exercitationem. Quae, sed!
        </p> */}
    </section>
  );
}
