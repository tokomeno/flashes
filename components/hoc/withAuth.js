import React, { Component } from "react";
import Router, { withRouter } from "next/router";

const withAuth = WrappedComponent => {
  return class hoc_withAuth extends Component {
    // https://www.udemy.com/awesome-nextjs-with-react-and-node-amazing-portfolio-app/learn/lecture/12693167#questions/6900880
    // wrappedCOmponentes getInitalProps is not called cause hoc is wrapping it so we have to call
    // this funciton manually and return its props to wrappedcompponent
    static async getInitialProps(args) {
      if (!args.reduxStore.getState().auth.isAuth) {
        if (process.browser) {
          Router.push("/register");
        } else {
          args.res.redirect("/register");
        }
      }
      const pageProps =
        (await WrappedComponent.getInitialProps) &&
        (await WrappedComponent.getInitialProps(args));
      return { ...pageProps };
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
};

export default withAuth;
