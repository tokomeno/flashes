import React from "react";
import PropTypes from "prop-types";
import { Link } from "../../routes";

const FlashMode = ({
  title,
  icon,
  square_address,
  font_size = {},
  showImage = true
}) => {
  icon = icon && false ? icon : "/static/images/demo.png";
  return (
    <Link route={square_address}>
      <div className={"flash__mode"} style={font_size}>
        {showImage && (
          <figure>
            <img src={icon} alt={title} />
          </figure>
        )}
        <span>{title}</span>
      </div>
    </Link>
  );
};

FlashMode.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
  square_address: PropTypes.string.isRequired
};

// Square.defaultProps = {
//   // type: 'text',
//   // disabled: '',
//   // info: false
// };

export default FlashMode;
