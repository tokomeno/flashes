import React, { Component } from "react";
import PropTypes from "prop-types";

import FlashMode from "./FlashMode";
const SquareContainer = props => {
  const flash_set = props.flash_set;
  return (
    <div className="square__container">
      <FlashMode
        square_address={`/flashes/${flash_set.id}/${flash_set.slug}`}
        title={"ფლეშქარდი"}
        icon={"/static/images/learn.png"}
        font_size={{ fontSize: "11px" }}
      />
      <FlashMode
        square_address={`/flashetest/${flash_set.id}/${flash_set.slug}`}
        title={"ტესტი"}
      />
      <FlashMode
        square_address={`/flashcards/${flash_set.id}/${flash_set.slug}`}
        title={"ყველა ფლეშქარდი"}
        showImage={false}
      />
    </div>
  );
};

SquareContainer.propTypes = {
  flash_set: PropTypes.object.isRequired
};

export default SquareContainer;
