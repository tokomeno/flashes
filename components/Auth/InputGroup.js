import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

const InputGroup = ({ name, type, placeholder, value, error, onChange }) => {
  return (
    <React.Fragment>
      {/* <span> */}
      <input
        onChange={onChange}
        id={name}
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        autoComplete="off"
        className="form"
      />
      <span className="underline" />
      {error && <p className="text-danger mt-m-20">{error.msg}</p>}
      {/* </span> */}
    </React.Fragment>
  );
};

InputGroup.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  error: PropTypes.string,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

InputGroup.defaultProps = {
  type: "text"
};

export default InputGroup;
