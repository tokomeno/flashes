import React from "react";
import FbLoginButton from "./FbLoginButton";

const SocialLogin = () => {
  return (
    <div className="social-btns">
      <FbLoginButton />
      <a className="btn__background google" href="#">
        <i className="fab fa-google  fa-icon" />
      </a>
    </div>
  );
};

export default SocialLogin;
