import React, { Component } from "react";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { API_URL } from "../../serverRoutes";
import axios from "axios";
import { setCurrentUser } from "../../redux/auth/authActions";

import { connect } from "react-redux";

class FbLoginButton extends Component {
  componentClicked = () => {
    console.log("componentClicked");
  };
  responseFacebook = res => {
    const userData = {
      accessToken: res.accessToken,
      facebookId: res.userID,
      name: res.name,
      nickname: res.nickname,
      email: res.email,
      imageUrl: res.url
    };
    axios
      .post(API_URL + "/auth/provider/facebook", { userData })
      .then(res => {
        this.props.setCurrentUser({
          user: res.data.user,
          token: res.data.token
        });
      })
      .catch(err => console.log(err));
  };

  render() {
    return (
      <div>
        <FacebookLogin
          appId="2998098066897242"
          autoLoad={false}
          fields="name,email,picture"
          onClick={this.componentClicked}
          callback={this.responseFacebook}
          render={renderProps => (
            <a
              onClick={renderProps.onClick}
              className="btn__background facebook"
              href="#"
            >
              <i className="fab fa-facebook-f  fa-icon" />
            </a>
          )}
        />
        ;
      </div>
    );
  }
}

export default connect(
  null,
  { setCurrentUser }
)(FbLoginButton);
