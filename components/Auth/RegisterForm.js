import React, { Component } from "react";
import { registerUser, authStartLoading } from "../../redux/auth/authActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import InputGroup from "./InputGroup";
import classnames from "classnames";
import SocialLogin from "./SocialLogin";

class RegisterForm extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      password2: "",
      nickname: ""
    };
  }
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const newUser = {
      name: this.state.name,
      email: this.state.email,
      nickname: this.state.nickname,
      password: this.state.password,
      password2: this.state.password2
    };
    this.props.authStartLoading();
    this.props.registerUser(newUser);
  };

  render() {
    const { errors } = this.props;
    1;
    return (
      <section className="show__form__class signup">
        <div className="login__container">
          <div className="signup-content">
            <div className="signup-form">
              <div className="singin__header">
                <SocialLogin />
              </div>

              <form onSubmit={this.onSubmit} className="pb-2 formClass">
                <div className="desc font-weight-bold">
                  <p>გთხოვთ გაიროთ რეგისტრაცია!</p>
                </div>
                <div className="signUp">
                  <span>
                    <InputGroup
                      name="nickname"
                      placeholder="nickname"
                      onChange={this.onChange}
                      value={this.state.nickname}
                      error={errors && errors.nickname}
                    />
                  </span>
                  <span>
                    <InputGroup
                      name="name"
                      placeholder="სახელი და გვარი"
                      onChange={this.onChange}
                      value={this.state.name}
                      error={errors && errors.name}
                    />
                  </span>
                  <span>
                    <InputGroup
                      type="email"
                      name="email"
                      placeholder="იმეილი"
                      onChange={this.onChange}
                      value={this.state.email}
                      error={errors && errors.email}
                    />
                  </span>
                  <span>
                    <InputGroup
                      type="password"
                      name="password"
                      placeholder="პაროლი"
                      onChange={this.onChange}
                      value={this.state.password}
                      error={errors && errors.password}
                    />
                  </span>

                  <span>
                    <InputGroup
                      type="password"
                      name="password2"
                      placeholder="პაროლი"
                      onChange={this.onChange}
                      value={this.state.password2}
                      error={errors && errors.password2}
                    />
                  </span>
                </div>

                <button
                  className={classnames("submit btn btn-light", {
                    regsending: this.props.loading
                  })}
                  type="submit"
                >
                  გაგაზავნა <i className="fas fa-paper-plane" />
                </button>
              </form>
            </div>
            <div className="signup-image">
              <figure>
                <img
                  src="/static/images/signup-image.jpg"
                  alt="sing up image"
                />
              </figure>
              <a
                ref="#"
                onClick={this.props.toggleForm}
                className="signup-image-link form__toggler"
              >
                ავტორიზაცია
              </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

RegisterForm.propTypes = {
  // registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    auth: state.auth,
    errors: state.errors,
    loading: state.auth.loading
  };
};
export default connect(
  mapStateToProps,
  { registerUser, authStartLoading }
)(RegisterForm);
