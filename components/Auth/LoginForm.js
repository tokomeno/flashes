import React, { Component } from "react";
import { loginUser, authStartLoading } from "../../redux/auth/authActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import InputGroup from "./InputGroup";
import classnames from "classnames";
import SocialLogin from "./SocialLogin";

class LoginForm extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: ""
    };
  }
  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const newUser = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.authStartLoading();
    this.props.loginUser(newUser);
  };

  render() {
    const { errors } = this.props;
    return (
      <section className="show__form__class sign-in">
        <div className="login__container">
          <div className="signin-content">
            <div className="signin-image">
              <figure>
                <img
                  src="/static/images/signin-image.jpg"
                  alt="sing up image"
                />
              </figure>
              <a
                ref="#"
                onClick={this.props.toggleForm}
                className="signup-image-link form__toggler"
              >
                რეგისტრაცია
              </a>
            </div>

            <div className="signin-form">
              <div className="singin__header">
                <h3 className="form-title">ავტორიზაცია</h3>
                <SocialLogin />
              </div>
              {/*  */}
              <form onSubmit={this.onSubmit} className="pb-2 formClass">
                <div className="desc font-weight-bold">
                  <p className="text-danger">{errors && errors.msg}</p>
                </div>
                <div className="signUp">
                  <span>
                    <InputGroup
                      type="email"
                      name="email"
                      placeholder="იმეილი"
                      onChange={this.onChange}
                      value={this.state.email}
                    />
                  </span>
                  <span>
                    <InputGroup
                      type="password"
                      name="password"
                      placeholder="პაროლი"
                      onChange={this.onChange}
                      value={this.state.password}
                    />
                  </span>
                </div>

                <div style={{ height: "40px" }}>
                  <button
                    className={classnames("submit btn btn-light", {
                      regsending: this.props.loading
                    })}
                    type="submit"
                  >
                    გაგაზავნა <i className="fas fa-paper-plane" />
                  </button>
                </div>
              </form>
              {/*  */}
            </div>
          </div>
        </div>
      </section>
    );
  }
}

// export default LoginForm;
const mapStateToProps = state => {
  return {
    auth: state.auth,
    errors: state.errors,
    loading: state.auth.loading
  };
};
export default connect(
  mapStateToProps,
  { loginUser, authStartLoading }
)(LoginForm);
