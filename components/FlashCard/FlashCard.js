import React, { Component } from "react";

import classnames from "classnames";
import PropTypes from "prop-types";

class FlashCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeBack: false
    };
    this.cardForntSide = React.createRef();
    this.cardBackSide = React.createRef();
  }

  setFontSize = domElement => {
    const textLength = domElement.textContent.length;
    const baseSize = 94;
    if (textLength >= baseSize) {
      const extraChars = textLength - baseSize;
      const fontSize = extraChars / 40;
      const newFontSize = `${26 - fontSize}px`;
      domElement.style.fontSize = newFontSize;
    }
  };

  componentDidMount() {
    this.setFontSize(this.cardForntSide.current);
    this.setFontSize(this.cardBackSide.current);
  }

  toggleCard = () => {
    if (window.getSelection().toString()) return;
    this.setState(prevState => ({ activeBack: !this.state.activeBack }));
  };
  render() {
    const { term, definition } = this.props.flashCard;
    const { coming_sides } = this.props;
    return (
      <div className={"card__wrapper"}>
        <div
          className={classnames("card", "card__slide_in", {
            flipme: this.state.activeBack,
            [`${coming_sides}`]: true
          })}
          onClick={this.toggleCard}
        >
          <div className="card__content">
            <div className="card__front">
              <p className="card__subtitle" ref={this.cardForntSide}>
                {term}
              </p>
            </div>

            <div className="card__back">
              <p className="card__subtitle" ref={this.cardBackSide}>
                {definition}{" "}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
FlashCard.propTypes = {
  flashCard: PropTypes.object.isRequired
};

export default FlashCard;
