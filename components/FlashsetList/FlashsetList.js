import React, { Component } from "react";
import { Link as NextLink } from "../../routes";
import InfiniteScroll from "react-infinite-scroll-component";
import axios from "axios";
import { API_URL } from "../../serverRoutes";

export default class FlashsetList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flashsets: [...this.props.flashsets],
      total: this.props.total,
      perPage: this.props.perPage ? this.props.perPage : 10
    };
  }
  fetchFlashsets = () => {
    const page = this.state.flashsets.length / this.state.perPage + 1;
    axios.get(`${API_URL}/flashsets?page=${page}`).then(res => {
      this.setState({
        flashsets: this.state.flashsets.concat(res.data.flashsets)
      });
    });
  };
  refresh = () => {};
  render() {
    return (
      <InfiniteScroll
        dataLength={this.state.flashsets.length}
        next={this.fetchFlashsets}
        hasMore={this.state.total > this.state.flashsets.length}
        loader={
          <div className="load-7">
            <div className="square-holder">
              <div className="square" />
            </div>
          </div>
        }
        style={{ overflow: "initial" }}
        className="row"
      >
        {this.state.flashsets.map(item => (
          <div className="col-md-6" key={item._id}>
            <FlashSet flashset={item} />
          </div>
        ))}
      </InfiniteScroll>
    );
  }
}

const FlashSet = props => {
  const flashcardCount = props.flashset.flashcards
    ? props.flashset.flashcards.length
    : 0;
  return (
    <div className="flashset__wrapper">
      <div className="flashset__head">
        <NextLink
          route="flashes"
          params={{ id: props.flashset._id, slug: props.flashset.slug }}
        >
          <a className="href__n">{props.flashset.title}</a>
        </NextLink>
      </div>
      <div className="term__counter">{flashcardCount} ფლეშქარდი</div>
      {props.flashset.creator && (
        <a
          href={"users/" + props.flashset.creator.nickname}
          className="href__n flashset__user"
        >
          {/* <img src={props.flashset.creator.avatar} /> */}
          {props.flashset.creator.nickname}
        </a>
      )}
    </div>
  );
};
