import Link from "next/link";
import Head from "next/head";
import "../styles/app.scss";
import React, { Component, useState } from "react";
import { connect } from "react-redux";
import { Link as NextLink } from "../routes";

import classnames from "classnames";
import Logo from "./comon/logo";
import { authLogout } from "../redux/auth/authActions";
import { toogleMenu } from "../redux/ui/uiActions";

// export default ({
//   children,
//   title = "This is the default title",
//   description = "flahser.ge ისწავლე ბლა ბლუნით"
// }) => (

class Layout extends Component {
  // constructor(props) {
  //   super(props);
  // }

  render() {
    const {
      children,
      title = "This is the default title",
      description = "flahser.ge ისწავლე ბლა ბლუნით"
    } = this.props;
    return (
      <div>
        <Head>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
            key="viewport"
          />
          <link rel="stylesheet" type="text/css" href="/static/nprogress.css" />
          <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
            integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
            crossOrigin="anonymous"
          />
          <title>{title}</title>
          <meta key="description" name="description" content={description} />
          <meta
            key="keywords"
            name="keywords"
            content="ეროვნული გამოცდები, ტესტები, აბიტურიენტები"
          />
        </Head>
        <Menu
          auth={this.props.auth}
          authLogout={this.props.authLogout}
          toogleMenu={this.props.toogleMenu}
          isMenuActive={this.props.isMenuActive}
        />
        {children}

        <footer>
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="navbar__brand">
                  <Link href="/">
                    <a>
                      <Logo />
                      <span>Flasher</span>
                    </a>
                  </Link>
                </div>
              </div>
              <div className="col-md-4 text-center">
                <div className="qoute">
                  <i>A Year From Now You Will Wish You Had Started Today</i>
                </div>
              </div>
              <div className="col-md-4 copyright">
                No copyright infringement intended
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuth: state.auth.isAuth,
  auth: state.auth,
  isMenuActive: state.ui.isMenuActive
});
export default connect(
  mapStateToProps,
  { authLogout, toogleMenu }
)(Layout);

const NotLoggedInLinks = () => (
  <React.Fragment>
    <li>
      <Link href="/login">
        <a className="link">ავტორიზაცია</a>
      </Link>
    </li>
    <li>
      <Link href="/register">
        <a className="link">რეგისტრაცია</a>
      </Link>
    </li>
  </React.Fragment>
);

const LoggedInLinks = props => (
  <React.Fragment>
    <li>
      <Link href="/flashsets/create">
        <a className="create__flashset link">
          შექმნა <i className="fas fa-plus-circle" />{" "}
        </a>
      </Link>
    </li>
    <li>
      <NextLink route={`/users/${props.user.nickname}`}>
        <a className="link">{props.user.nickname}</a>
      </NextLink>
    </li>
    <li>
      <a onClick={props.logout} className="link">
        გამოსვლა
      </a>
    </li>
  </React.Fragment>
);

const Menu = ({ auth, authLogout, toogleMenu, isMenuActive }) => {
  return (
    <header>
      <div className="navbar__wrapper">
        <div className="navbar__container container">
          <div className="navbar__brand">
            <Link href="/">
              <a>
                <Logo />
                <span
                  className={classnames("logoText", {
                    ["text-white"]: isMenuActive
                  })}
                >
                  Flasher
                </span>
              </a>
            </Link>
          </div>
          <button
            id="navbar-btn"
            type="button"
            className={classnames({ change: isMenuActive })}
            onClick={toogleMenu}
          >
            <div className="bar1"></div>
            <div className="bar2"></div>
            <div className="bar3"></div>
          </button>
          <nav
            id="navbar__nav"
            className={classnames({ isActive: isMenuActive })}
          >
            <ul className="navbar__menu">
              <li>
                <Link href="/">
                  <a className="link">მთავარი</a>
                </Link>
              </li>
              {!auth.user && <NotLoggedInLinks />}
              {auth.user && (
                <LoggedInLinks user={auth.user} logout={authLogout} />
              )}
            </ul>
          </nav>
        </div>
      </div>
    </header>
  );
};
