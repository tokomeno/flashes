import React, { Component } from "react";
import PropTypes from "prop-types";
import Question from "./Question";
import Option from "./Option";
import { connect } from "react-redux";
import { flashcard_was_answered } from "../../redux/flashcards/flashcardActions";
import Preview from "./Preview";
import Logo from "../../components/comon/logo";

// Array.prototype.first = function() {
//   return this.find(x=>x!==undefined);
// };

class QuizContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      answeredFlashcard: null,
      isPreview: false,
      userAnswer: null,

      currentFlashcard: null,
      finished: null,
      options: null
    };
  }
  componentDidMount() {
    let currentFlashcard = this.findNextQuestion(null);
    let options = currentFlashcard ? this.getOptions(currentFlashcard) : [];

    this.setState({
      currentFlashcard: currentFlashcard,
      finished: !!!currentFlashcard,
      options: options
    });
  }
  findNextQuestion = prevFlashcard => {
    return this.props.flashcards
      .filter(flash => {
        if (prevFlashcard && this.props.flashcards.length > 1) {
          return flash.id !== prevFlashcard.id && flash.correct_count < 2;
        }
        return flash.correct_count < 2;
      })
      .sort(() => 0.5 - Math.random())
      .find(x => x !== undefined);
  };
  getOptions = currentFlashcard => {
    let options = this.props.flashcards.filter(
      item => item.id != currentFlashcard.id
    );
    options.sort(() => 0.5 - Math.random());
    options = options.slice(0, 3);
    options.push(currentFlashcard);
    options.sort(() => 0.5 - Math.random());
    return options;
  };
  checkAnswer = () => {
    if (!this.state.userAnswer) {
      alert("choose answer");
      return;
    }
    const correct =
      this.state.userAnswer.id == this.state.currentFlashcard.id ? 1 : 0;
    this.props.flashcard_was_answered(this.state.currentFlashcard.id, correct);
    this.setState({
      answeredFlashcard: this.state.userAnswer,
      isPreview: true
    });
  };
  setAnswer = answer => {
    this.setState(
      {
        userAnswer: answer
      },
      this.checkAnswer
    );
  };
  nextQuestion = () => {
    let currentFlashcard = this.findNextQuestion(this.state.currentFlashcard);
    let options = currentFlashcard ? this.getOptions(currentFlashcard) : [];
    this.setState({
      finished: !!!currentFlashcard,
      currentFlashcard: currentFlashcard,
      options: options,
      isPreview: false,
      userAnswer: null
    });
  };

  render() {
    const QuizResut = <div className="reult__container" />;
    if (!this.state.options) {
      return null;
    }
    return (
      <section className="d-flex">
        <div className="question__container">
          {this.state.finished ? (
            QuizResut
          ) : this.state.isPreview ? (
            <Preview
              currentFlashcard={this.state.currentFlashcard}
              answeredFlashcard={this.state.answeredFlashcard}
              nextQuestion={this.nextQuestion}
            />
          ) : (
            <div className="anime-fade-in-right">
              <Question currentFlashcard={this.state.currentFlashcard} />
              <hr />
              {this.state.options.map(item => {
                return (
                  <Option
                    key={item.id}
                    flashcard={item}
                    currentFlashcard={this.state.currentFlashcard}
                    answeredFlashcard={this.state.answeredFlashcard}
                    isPreview={this.state.isPreview}
                    setAnswer={this.setAnswer}
                    userAnswerId={
                      this.state.userAnswer ? this.state.userAnswer.id : null
                    }
                  />
                );
              })}
            </div>
          )}
        </div>
        <div className="flash__stats anime-top-to-bottom">
          <div className="flash__stats__item">
            ყველა: {this.props.flashcards.length}
          </div>
          <div className="flash__stats__item">
            სასწავლი: {this.props.remaining.length}
          </div>
          <div className="flash__stats__item">
            ნაცნობი: {this.props.familiar.length}
          </div>
          <div className="flash__stats__item">
            ნასწავლი: {this.props.mastered.length}
          </div>
          <div className="d-flex justify-content-center mt-3">
            <Logo width="80px" height="80px" />
          </div>
        </div>
      </section>
    );
  }
}

QuizContainer.propTypes = {
  flashcards: PropTypes.array.isRequired,
  remaining: PropTypes.array.isRequired,
  familiar: PropTypes.array.isRequired,
  mastered: PropTypes.array.isRequired
};

export default connect(
  null,
  { flashcard_was_answered }
)(QuizContainer);
