import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

const option = React.memo(
  ({
    flashcard,
    currentFlashcard,
    answeredFlashcard,
    isPreview,
    setAnswer,
    userAnswerId
  }) => {
    return (
      <div
        className={classnames(
          "option",
          {
            "is-wrong":
              isPreview &&
              currentFlashcard.id !== flashcard.id &&
              answeredFlashcard.id == flashcard.id
          },
          {
            "is-correct": isPreview && currentFlashcard.id == flashcard.id
          }
        )}
      >
        <input
          className="quiz__options"
          onChange={() => setAnswer(flashcard)}
          type="radio"
          id={"oL__" + flashcard.id}
          name="quiz_option"
          checked={flashcard.id == userAnswerId}
          value={flashcard.id}
        />
        <label htmlFor={"oL__" + flashcard.id}>
          {" "}
          {flashcard.definition}
          {flashcard.id == currentFlashcard.id && " ---correct"}
        </label>
      </div>
    );
  }
);
export default option;

option.propTypes = {
  flashcard: PropTypes.object.isRequired, // from where option is taken
  currentFlashcard: PropTypes.object.isRequired, // quesiton of flashcard
  answeredFlashcard: PropTypes.object, // user answer def of this flashcard,
  isPreview: PropTypes.bool.isRequired,
  setAnswer: PropTypes.func.isRequired
};
