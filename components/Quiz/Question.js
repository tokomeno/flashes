import React from "react";
import PropTypes from "prop-types";

const Question = React.memo(({ currentFlashcard }) => {
  // console.log(props);
  return <div className="question">{currentFlashcard.term}</div>;
});

export default Question;

Question.propTypes = {
  currentFlashcard: PropTypes.object.isRequired
};
