import React, { Component } from "react";

import classnames from "classnames";
import PropTypes from "prop-types";
const Term = props => {
  const { term, definition, correct_count, wrong_count, id } = props.term;
  return (
    <div className="term__container">
      <div className="term__counter">
        <span> {correct_count}</span>
        <span>{wrong_count}</span>
        {/* <span>{id}</span> */}
      </div>
      <div className="left">{term}</div>
      <div className="right">{definition}</div>
    </div>
  );
};

Term.propTypes = {
  term: PropTypes.object.isRequired
};

export default React.memo(Term);
