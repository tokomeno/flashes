import React, { Component } from "react";
import Term from "./Term";

import PropTypes from "prop-types";
class TermList extends Component {
  render() {
    const { remaining, familiar, mastered } = this.props;
    return (
      <React.Fragment>
        {mastered.length > 0 && (
          <div>
            <h6 className="mt-3">mastered</h6>
            {mastered.map(term => {
              return <Term term={term} key={term.id} />;
            })}
          </div>
        )}
        {familiar.length > 0 && (
          <div>
            <h6 className="mt-3">familiar</h6>
            {familiar.map(term => {
              return <Term term={term} key={term.id} />;
            })}
          </div>
        )}
        {remaining.length > 0 && (
          <div>
            <h6 className="mt-3">remaining</h6>
            {remaining.map(term => {
              return <Term term={term} key={term.id} />;
            })}
          </div>
        )}
      </React.Fragment>
    );
  }
}
TermList.propTypes = {
  terms: PropTypes.array.isRequired,
  remaining: PropTypes.array.isRequired,
  terms: PropTypes.array.isRequired,
  terms: PropTypes.array.isRequired
};

export default TermList;
