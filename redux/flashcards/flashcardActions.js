import { SET_FLASHCARDS, FLASHCARD_WAS_ANSWERED } from "./flashcardTypes";
import {
  join_FC_answer_count,
  filter_flashcards
} from "../../helpers/format_flashes";

export function flashcard_was_answered(flashcard_id, correct) {
  return { type: FLASHCARD_WAS_ANSWERED, payload: { flashcard_id, correct } };
}

export function set_flashset({ flashset, flashcards }) {
  const flash_set = {
    flashcards: flashcards.map(card => ({ id: card._id, ...card })),
    id: flashset._id,
    slug: flashset.slug,
    title: flashset.title,
    user_answers: [
      // { flash_id: 1, correct_count: 9, wrong_count: 3 }
      // { flash_id: 2, correct_count: 1, wrong_count: 3 },
      // { flash_id: 3, correct_count: 3, wrong_count: 1 }
    ],
    joined_flashcards: {}
  };
  flash_set.joined_flashcards = join_FC_answer_count(
    flash_set.flashcards,
    flash_set.user_answers
  );
  const {
    flashcards: join_flashcards,
    remaining,
    familiar,
    mastered
  } = filter_flashcards(flash_set.joined_flashcards);
  return {
    type: SET_FLASHCARDS,
    payload: {
      flash_set,
      flashcards: join_flashcards,
      remaining,
      familiar,
      mastered
    }
  };
}
