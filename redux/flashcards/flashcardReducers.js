import { filter_flashcards } from "../../helpers/format_flashes";
import { SET_FLASHCARDS, FLASHCARD_WAS_ANSWERED } from "./flashcardTypes";
const initState = {
  flash_set: null,
  flashcards: null,
  remaining: null,
  familiar: null,
  mastered: null
};

export const flashReducer = (state = initState, action) => {
  switch (action.type) {
    case SET_FLASHCARDS:
      return {
        ...state,
        flash_set: action.payload.flash_set,
        flashcards: action.payload.flashcards,
        remaining: action.payload.remaining,
        familiar: action.payload.familiar,
        mastered: action.payload.mastered
      };
    case FLASHCARD_WAS_ANSWERED:
      const new_flashcards = state.flash_set.joined_flashcards.map(card => {
        if (card.id == action.payload.flashcard_id) {
          if (action.payload.correct) {
            card.correct_count++;
          } else {
            card.wrong_count++;
          }
        }
        return card;
      });
      const { flashcards, remaining, familiar, mastered } = filter_flashcards(
        new_flashcards
      );
      return {
        ...state,
        flash_set: {
          ...state.flash_set,
          joined_flashcards: new_flashcards
        },
        remaining,
        flashcards,
        familiar,
        mastered
      };
    default:
      return state;
  }
};
