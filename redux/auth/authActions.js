import Axios from "axios";
import { API_URL } from "../../serverRoutes";
import {
  LOGIN_USER,
  LOGOUT_USER,
  AUTH_STOP_LOAD,
  AUTH_START_LOADING
} from "./authTypes";
import { GET_ERRORS } from "../errors/errorsTypes";
import jwt_decode from "jwt-decode";
import Cookies from "js-cookie";
import { setCookie } from "../../helpers/utils";
import Router from "next/router";
import axios from "axios";

export const setCurrentUser = ({ user, token }) => {
  const decoded = jwt_decode(token);
  // Cookies.set("user", user);
  setCookie("user", JSON.stringify(user));
  Cookies.set("token", token);
  Cookies.set("expiresAt", decoded.exp);
  // Check for expired token
  Router.push("/");
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  return {
    type: LOGIN_USER,
    payload: { user, token }
  };
};

export const authLogout = () => {
  Cookies.remove("user");
  Cookies.remove("token");
  Cookies.remove("expiresAt");
  return {
    type: LOGOUT_USER
  };
};
export const authStartLoading = () => {
  return {
    type: AUTH_START_LOADING
  };
};

export const loginUser = credentials => dispatch => {
  Axios.post(API_URL + "/auth/login", credentials)
    .then(res => {
      return dispatch(
        setCurrentUser({
          user: res.data.user,
          token: res.data.token
        })
      );
    })
    .catch(err => {
      dispatch({ type: AUTH_STOP_LOAD });
      dispatch({
        type: GET_ERRORS,
        payload: {
          msg: "მონაცემები არასწორია სცადეთ თავდან"
        }
      });
    });
};

export const registerUser = userData => dispatch => {
  Axios.post(API_URL + "/auth/register", userData)
    .then(res => {
      return dispatch(
        setCurrentUser({
          user: res.data.user,
          token: res.data.token
        })
      );
    })
    .catch(err => {
      dispatch({ type: AUTH_STOP_LOAD });
      return dispatch({
        type: GET_ERRORS,
        payload: err.response.data.errors
      });
    });
};
