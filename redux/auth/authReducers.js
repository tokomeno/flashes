import {
  LOGIN_USER,
  LOGOUT_USER,
  AUTH_STOP_LOAD,
  AUTH_START_LOADING,
  SET_CURRENT_USER
} from "./authTypes";

const initState = {
  isAuth: false,
  token: null,
  user: null,
  loading: false
};

export const authReducer = (state = initState, action) => {
  switch (action.type) {
    case LOGIN_USER:
      return {
        ...state,
        isAuth: true,
        loading: false,
        user: action.payload.user,
        token: action.payload.token
      };
    case LOGOUT_USER:
      return {
        ...state,
        isAuth: false,
        user: null,
        token: null
      };
    case AUTH_STOP_LOAD:
      return {
        loading: false
      };
    case AUTH_START_LOADING:
      return {
        loading: true
      };
    default:
      return state;
  }
};
