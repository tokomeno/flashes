import { TOOGLE_MENU, HIDE_MENU } from "./uiTypes";
const initState = {
  isMenuActive: false
};

export const uiReducer = (state = initState, action) => {
  switch (action.type) {
    case TOOGLE_MENU:
      return {
        ...state,
        isMenuActive: !state.isMenuActive
      };
    case HIDE_MENU:
      return {
        ...state,
        isMenuActive: false
      };
    default:
      return state;
  }
};
