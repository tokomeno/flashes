import { TOOGLE_MENU, HIDE_MENU } from "./uiTypes";

export const toogleMenu = () => {
  return {
    type: TOOGLE_MENU
  };
};

export const hideMenu = () => {
  return {
    type: HIDE_MENU
  };
};
