import { GET_ERRORS } from "./errorsTypes";

const initState = {};

export const errorReducer = function(state = initState, action) {
  switch (action.type) {
    case GET_ERRORS:
      return action.payload;
    default:
      return state;
  }
};
