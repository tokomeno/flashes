// src/Auth/Auth.js
import auth0 from "auth0-js";
import Cookies from "js-cookie";
import jwt from "jsonwebtoken";
import axios from "axios";

import { getCookesFromReq } from "../helpers/utils";

class Auth {
  constructor() {
    this.auth0 = new auth0.WebAuth({
      domain: "tokomeno.eu.auth0.com",
      clientID: "HjLJfpQcy9rVYjpgPgL2kvubbRI1vOJ6",
      redirectUri: "http://localhost:3000/callback",
      responseType: "token id_token",
      // scope: "openid",
      scope: "openid profile"
    });
    this.verifyToken = this.verifyToken.bind(this);
    this.clientAuth = this.clientAuth.bind(this);
  }
  handleAuthentication = () => {
    return new Promise((reslove, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          this.setSession(authResult);
          reslove();
        } else if (err) {
          // history.replace("/home");
          console.log(err);
          alert(`Error: ${err.error}. Check the console for further details.`);
          reject();
        }
      });
    });
  };

  setSession(authResult) {
    // debugger;

    // Set the time that the Access Token will expire at
    let expiresAt = authResult.expiresIn * 1000 + new Date().getTime();

    Cookies.set("user", authResult.idTokenPayload);
    Cookies.set("jwt", authResult.idToken);
    Cookies.set("expiresAt", expiresAt);
  }

  logout = () => {
    Cookies.remove("user");
    Cookies.remove("jwt");
    Cookies.remove("expiresAt");

    this.auth0.logout({
      returnTo: "",
      clientID: "HjLJfpQcy9rVYjpgPgL2kvubbRI1vOJ6"
    });
  };

  async getJWKS() {
    const res = await axios.get(
      "https://tokomeno.eu.auth0.com./.well-known/jwks.json"
    );
    const jwks = res.data;
    return jwks;
  }

  async verifyToken(token) {
    if (token) {
      const decodedToken = jwt.decode(token, { complete: true });
      if (!decodedToken) {
        return undefined;
      }
      const jwks = await this.getJWKS();
      const jwk = jwks.keys[0];
      // BUILD CERTIFICATE
      let cert = jwk.x5c[0];
      cert = cert.match(/.{1,64}/g).join("\n");
      cert = `-----BEGIN CERTIFICATE-----\n${cert}\n-----END CERTIFICATE-----\n`;
      //
      if (jwk.kid === decodedToken.header.kid) {
        try {
          const verifiedToken = jwt.verify(token, cert);
          const expiresAt = verifiedToken.exp * 1000;
          if (verifiedToken && new Date().getTime() < expiresAt) {
            return verifiedToken;
          }
        } catch (err) {
          return undefined;
        }
      }
    }
    return undefined;
  }

  isAuthenticated = () => {
    let expiresAt = Cookies.getJSON("expiresAt");
    return new Date().getTime() < expiresAt;
  };

  async clientAuth() {
    // return this.isAuthenticated();
    const token = Cookies.getJSON("jwt");
    const verifiedToken = await this.verifyToken(token);
    console.log(!!verifiedToken);
    return verifiedToken;
  }
  serverAuth = req => {
    if (req.headers.cookie) {
      const token = getCookesFromReq(req, "jwt");
      const verifiedToken = this.verifyToken(token);
      return verifiedToken;
    }
    return undefined;
  };

  login = () => {
    this.auth0.authorize();
  };
}
const auth0Client = new Auth();
export default auth0Client;
