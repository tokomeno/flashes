const join_FC_answer_count = (flashcards, user_answers) => {
  const flashcards_withCount = flashcards.map(card => {
    let correct_count = 0;
    let wrong_count = 0;
    const u_answer = user_answers.filter(u_a => u_a.flash_id == card.id);
    if (u_answer.length) {
      correct_count = u_answer[0].correct_count;
      wrong_count = u_answer[0].wrong_count;
    }
    return {
      ...card,
      correct_count: correct_count,
      wrong_count: wrong_count
    };
  });
  return flashcards_withCount;
};

const filter_flashcards = flashcards => {
  const remaining = flashcards.filter(card => card.correct_count == 0);
  const familiar = flashcards.filter(card => card.correct_count == 1);
  const mastered = flashcards.filter(card => card.correct_count >= 2);
  return {
    flashcards,
    remaining,
    familiar,
    mastered
  };
};

export { filter_flashcards, join_FC_answer_count };
