import { createStore, applyMiddleware, combineReducers } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { flashReducer } from "./redux/flashcards/flashcardReducers";
import { authReducer } from "./redux/auth/authReducers";
import { errorReducer } from "./redux/errors/errorsReducers";
import { uiReducer } from "./redux/ui/uiReducers";

import thunkMiddleware from "redux-thunk";

const rootReducer = combineReducers({
  flash: flashReducer,
  auth: authReducer,
  errors: errorReducer,
  ui: uiReducer
});

export function initializeStore(initialState = {}) {
  return createStore(
    rootReducer,
    initialState,
    // composeWithDevTools(applyMiddleware(thunkMiddleware))
    composeWithDevTools(applyMiddleware(thunkMiddleware))
    // applyMiddleware(thunkMiddleware)
  );
}
