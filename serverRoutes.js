let serverUrl;
if (process.env.NODE_ENV == "production") {
  serverUrl = "https://flashesback.herokuapp.com";
} else {
  serverUrl = "http://localhost:8000";
}

export const API_URL = serverUrl;

export const saveFlashcardsUrl = () => ({
  path: serverUrl + "/flashcards",
  method: "post"
});

export const fetchFlashsetsUrl = ({ id }) => ({
  path: serverUrl + `/flashsets/${id}`,
  method: "get",
  params: {}
});

export const deleteFlashcardsUrl = ({ id }) => ({
  path: serverUrl + `/flashcards/${id}`,
  method: "delete",
  params: {}
});

export const deleteFlashsetUrl = ({ id }) => ({
  path: serverUrl + `/flashsets/${id}`,
  method: "delete",
  params: {}
});

export const userProfileUrl = ({ id }) => ({
  path: serverUrl + `/profile/${id}`,
  method: "get",
  params: {}
});

// export const SERVER_ROUTES = {
//   relocation: (r: Relocation) => `${environment.apiBase}/relocations/${r.id}`,
//   relocations: () => `${environment.apiBase}/relocations/`,
//   lastRelocation: () => `${environment.apiBase}/relocations/last`,
//   submitRelocation: (r: Relocation) =>
//     `${environment.apiBase}/relocations/${r.id}/submit`,
//   relocationPdf: (r: Relocation, p: Pdf) =>
//     `${environment.apiBase}/relocations/${r.id}/${p.path}`,
//   relocationPdfUpload: (r: Relocation, p: Pdf) =>
//     `${environment.apiBase}/relocations/${r.id}/pdfs/${p.id}/upload`,
//   sender: (r: Relocation) =>
//     `${environment.apiBase}/relocations/${r.id}/senders`,
//   pets: (p: Pet) => `${environment.apiBase}/pets/${p.id ? p.id : "{{id}}"}`,
//   adminUser: (u: User | number) =>
//     `${environment.apiBase}/admin/users/${typeof u === "number" ? u : u.id}`,
//   adminUsers: () => `${environment.apiBase}/admin/users`,
//   adminRelocation: (r: Relocation | number) =>
//     `${environment.apiBase}/admin/relocations/${
//       typeof r === "number" ? r : r.id
//     }`,
//   adminRelocations: () => `${environment.apiBase}/admin/relocations`,
//   mockmocc: (r: Relocation) =>
//     `${environment.apiBase}/admin/relocations/${r.id}/mockmocc`,
//   countries: () => `${environment.apiBase}/countries/`,
//   airports: () => `${environment.apiBase}/airports/`,
//   vaccines: () => `${environment.apiBase}/vaccines/`
// };
