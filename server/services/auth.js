const jwt = require("express-jwt");
const jwksClient = require("jwks-rsa");

const client = jwksClient.expressJwtSecret({
  cache: true,
  jwksUri: "https://tokomeno.eu.auth0.com./.well-known/jwks.json"
});

module.exports = checkJWT = jwt({
  secret: client,
  audience: "HjLJfpQcy9rVYjpgPgL2kvubbRI1vOJ6",
  issuer: "https://tokomeno.eu.auth0.com/",
  algorithms: ["RS256"]
});

// module.exports = checkJWT = function(req, res, next) {
//   const isValidToken = true;
//   if (isValidToken) {
//     next();
//   } else {
//     return res.status(401).json({ msg: "please log in" });
//   }
// };
