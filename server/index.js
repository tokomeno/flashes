const express = require("express");
const next = require("next");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const routes = require("../routes");
const nextRouterHandler = routes.getRequestHandler(app);

const isAuth = require("./services/auth");
app
  .prepare()
  .then(() => {
    const server = express();

    server.get("/api/secret", isAuth, (req, res) => {
      return res.json({ some: "som data", t: "sadf" });
    });

    // server.get("/flashes/:id/:slug", (req, res) => {
    //   const actualPage = "/flashes";
    //   const queryParams = { id: req.params.id, slug: req.params.slug };
    //   app.render(req, res, actualPage, queryParams);
    // });

    server.get("*", (req, res) => {
      return nextRouterHandler(req, res);
    });

    server.use(function(err, req, res, next) {
      if (err.name === "UnauthorizedError") {
        res.status(401).send("invalid token...");
      }
    });

    // server.use((error, req, res, next) => {
    //   console.log(error);
    //   console.trace(error);
    //   res
    //     .status(error.statusCode || 500)
    //     .json({ message: error.message, data: error.data });
    // });

    server.use(nextRouterHandler).listen(4200, err => {
      if (err) throw err;
      console.log(process.env.NODE_ENV, "> Ready on http://localhost:4200");
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    // process.exit(1);
  });
