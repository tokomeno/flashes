import { withRouter } from "next/router";
import Layout from "../components/layout";
import axios from "axios";

import FlashModeContainer from "../components/SetType/FlashModeContainer";
import QuizContainer from "../components/Quiz/QuizContainer";
import UserFlashInfo from "../components/UserFlashInfo";

import { connect } from "react-redux";
import { set_flashset } from "../redux/flashcards/flashcardActions";
import { fetchFlashsetsUrl } from "../serverRoutes";

class FlashesPage extends React.Component {
  static async getInitialProps({ reduxStore, req, query }) {
    const flash_set = reduxStore.getState().flash.flash_set;
    if (!flash_set || (flash_set && flash_set.slug != query.slug)) {
      const url = fetchFlashsetsUrl({ id: query.id });
      let response;
      try {
        response = await axios({
          method: url.method,
          url: url.path
        });
        reduxStore.dispatch(
          set_flashset({
            flashcards: response.data.flashcards,
            flashset: response.data.flashset
          })
        );
      } catch (err) {
        console.log("Cant Fetch FLashser: ", err);
      }
    }
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Layout
        title={this.props.flash_set.title + " - flasher.ge"}
        description={"this is form pahe bro"}
      >
        {this.props.flash_set && (
          <div className="container">
            <div className="row">
              <div className="col-md-12  text-center">
                <h1 className="pl-1 flashset__title">
                  {this.props.flash_set.title}
                </h1>
                <FlashModeContainer flash_set={this.props.flash_set} />
                <div className="quiz__container">
                  <QuizContainer
                    flashcards={this.props.flash_set.joined_flashcards}
                    remaining={this.props.remaining}
                    familiar={this.props.familiar}
                    mastered={this.props.mastered}
                  />
                </div>
                {/* <UserFlashInfo /> */}
              </div>
            </div>
          </div>
        )}
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    flash_set: state.flash.flash_set,
    mastered: state.flash.mastered,
    remaining: state.flash.remaining,
    familiar: state.flash.familiar
  };
};

export default connect(mapStateToProps)(withRouter(FlashesPage));
