import Layout from "../components/layout";

// import { withRouter } from "next/router";
import FlashCardSlider from "../components/FlashCard/FlashCardSlider";
import TermList from "../components/Term/TermList";
import FlashModeContainer from "../components/SetType/FlashModeContainer";
import axios from "axios";
import UserFlashInfo from "../components/UserFlashInfo";
import { connect } from "react-redux";
import { set_flashset } from "../redux/flashcards/flashcardActions";
import { fetchFlashsetsUrl } from "../serverRoutes";

class FlashesPage extends React.Component {
  static async getInitialProps({ reduxStore, req, query }) {
    const flash_set = reduxStore.getState().flash.flash_set;
    if (!flash_set || (flash_set && flash_set.slug != query.slug)) {
      const url = fetchFlashsetsUrl({ id: query.id });
      let response;
      try {
        response = await axios({
          method: url.method,
          url: url.path
        });
        reduxStore.dispatch(
          set_flashset({
            flashcards: response.data.flashcards,
            flashset: response.data.flashset
          })
        );
      } catch (err) {
        console.log(err);
      }
    }
    return {};
  }

  render() {
    return (
      <Layout title={this.props.flash_set.title + " - flasher.ge"}>
        <div className="container">
          <div className="row">
            <div className="col-md-9  text-center">
              <h1 className="pl-1 flashset__title  text-center">
                {this.props.flash_set.title}
              </h1>
              {/* <div className="d-flex"> */}
              <FlashModeContainer flash_set={this.props.flash_set} />
              <div className="flashcard__slider">
                <FlashCardSlider
                  flashCards={this.props.flash_set.joined_flashcards}
                />
              </div>
              {/* </div> */}
              <UserFlashInfo />
            </div>
          </div>
          <div className="row">
            <div className="col-md-9">
              <TermList
                remaining={this.props.remaining}
                familiar={this.props.familiar}
                mastered={this.props.mastered}
                terms={this.props.flash_set.joined_flashcards}
              />
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}
const mapStateToProps = state => ({
  flash_set: state.flash.flash_set,
  mastered: state.flash.mastered,
  remaining: state.flash.remaining,
  familiar: state.flash.familiar
});

export default connect(mapStateToProps)(FlashesPage);
