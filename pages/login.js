import Layout from "../components/layout";
import React from "react";
import { withRouter } from "next/router";

import LoginForm from "../components/Auth/LoginForm";
import RegisterForm from "../components/Auth/RegisterForm";

class Login extends React.Component {
  static async getInitialProps(ctx) {
    console.log(ctx.reduxStore.getState().auth.isAuth);
    if (ctx.reduxStore.getState().auth.isAuth) {
      try {
        if (process.browser) {
          Router.push("/");
        } else {
          ctx.res.redirect("/");
        }
      } catch (err) {
        console.error(err);
      }
    }
    return {};
  }
  constructor(props) {
    super(props);
    this.state = {
      showLogin: true
    };
  }
  toggleFormHandler = () => {
    this.setState(prevState => ({ showLogin: !this.state.showLogin }));
  };
  async componentDidMount() {}
  render() {
    return (
      <Layout title="ავტორიზაცია - flasher.ge">
        <div className="mt-3 login__container">
          {this.state.showLogin ? (
            <LoginForm toggleForm={this.toggleFormHandler} />
          ) : (
            <RegisterForm toggleForm={this.toggleFormHandler} />
          )}
        </div>
      </Layout>
    );
  }
}

export default withRouter(Login);
