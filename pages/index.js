import axios from "axios";
import Layout from "../components/layout";
import React from "react";
import { connect } from "react-redux";
import { Link as NextLink } from "../routes";
import FlashSet from "../components/FlashsetList/FlashsetList";
import { API_URL } from "../serverRoutes";

class Index extends React.Component {
  static async getInitialProps({ reduxStore, req, query }) {
    try {
      const response = await axios.get(API_URL + "/flashsets");
      return {
        flashsets: response.data.flashsets,
        total: response.data.total
      };
    } catch (err) {
      console.log("error fetdhin flashsets: ", err);
      return { flashsets: [] };
    }
  }

  componentDidMount() {
    document.addEventListener("scroll", this.rotateElements, true);
    this.fl__image = document.querySelectorAll(".fl__image");
    this.fl__cards = document.querySelectorAll(".fl__wraper");
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.rotateElements, true);
  }
  rotateElements = e => {
    let posY = window.pageYOffset || document.documentElement.scrollTop;
    if (posY > 300) return;
    posY = posY / 50;
    this.fl__image[0].style.transform = "rotate(" + posY * 2 * -1 + "deg)";
    [
      ["translate3d(18.65px, 41.65px, 0px)  scale(1)", -6.005],
      ["translate3d(6.65px, 38.65px, 0px)  scale(1)", 0.67],
      ["translate3d(7.1746px, 12.1746px, 0px) scale(1)", -2.15238],
      ["translate3d(15.65px, 22.65px, 0px)  scale(1)", 1.005],
      ["translate3d(6.65px, 26.65px, 0px) scale(1)", -1.995]
    ].map((item, index) => {
      let newDeg = (item[1] + posY) / (index + 1);
      this.fl__cards[index].style.transform =
        item[0] + "rotate(" + newDeg + "deg)";
    });
  };
  render() {
    return (
      <Layout title="ისწავლე ფლეშქარდებით მარტივად - flasher.ge">
        <div className="home__container">
          <div className="left">
            <h4 className="title">ისწავლე ფლეშქარდებით</h4>
            <p className="body">
              {" "}
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore,
              repudiandae.{" "}
            </p>
          </div>
          <div className="right">
            <div className="fl__image">
              <img src="/static/images/fc-mastery.svg" />
            </div>
            {[1, 2, 3, 4, 5].map(item => {
              return (
                <div className="fl__wraper" key={item}>
                  <div className="fl__head" />
                  <div className="fl__body">
                    <div className="term">ფლეშქარდი</div>
                    <div className="definition">
                      შექმენი შენი ფლეშქარდი მარტივად
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="container">
          <div className="flashsets__container">
            <div className="row">
              <div className="col-md-8">
                <FlashSet
                  flashsets={this.props.flashsets}
                  total={this.props.total}
                />
              </div>
            </div>
          </div>
        </div>
        <div style={{ height: "250px" }} />
      </Layout>
    );
  }
}

export default Index;

// const para = e => {
// var MourPositionX = Math.floor((e.clientX * 100) / window.innerWidth);
// var MourPositionY = Math.floor((e.clientY * 100) / window.innerHeight);
// var bb = (50 - MourPositionX) / 5;
// var YY = -MourPositionY / 10;
// var nnn = 50 - YY;
// const target = document.querySelectorAll(".fl__wraper");
// for (let index = 0; index < target.length; index++) {
//   target[index].style.transform = "translate( " + (bb - 50) + "%,-" + nnn + "%)";
//   // target[index].style.transform = "rotate(" + 13 + "deg)";
// }
//   transform: " translate( " + (bb - 50) + "%,-" + nnn + "%)"
//   transform: " translate( " + (bb - 55) + "%,-" + nnn + "%)"
// };
