import Layout from "../components/layout";
import React from "react";
import auth0Client from "../services/auth0";
import { withRouter } from "next/router";

class CallbackPage extends React.Component {
  async componentDidMount() {
    await auth0Client.handleAuthentication();
    this.props.router.push("/");
  }
  render() {
    return (
      <Layout title="ისწავლე ფლეშქარდებით მარტივად - flasher.ge">
        <div>loging</div>
      </Layout>
    );
  }
}

export default withRouter(CallbackPage);
