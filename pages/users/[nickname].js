// import withAuth from "../components/hoc/withAuth";
import React, { Component, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import Layout from "../../components/layout";
import { userProfileUrl, deleteFlashsetUrl } from "../../serverRoutes";
import moment from "moment";
import { Link as NextLink, Router } from "../../routes";
import { connect } from "react-redux";

class Porfile extends Component {
  static async getInitialProps({ reduxStore, req, query }) {
    const { nickname } = query;

    const url = userProfileUrl({ id: nickname });
    const response = await axios({ url: url.path, method: url.method });

    return {
      profileUser: response.data.user,
      flashsets: response.data.flashsets
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      flashsets: props.flashsets
    };
  }

  redirectToFlashsetEdit = (e, flashId) => {
    e.stopPropagation();
    Router.pushRoute("flashsetEdit", {
      id: flashId
    });
  };

  deleteFlashset = (e, flashId) => {
    e.stopPropagation();
    const url = deleteFlashsetUrl({ id: flashId });
    axios({ url: url.path, method: url.method })
      .then(res => {
        this.setState({
          flashsets: this.state.flashsets.filter(f => f._id !== flashId)
        });
      })
      .catch(err => {
        console.log("failed delete: ", err);
      });
  };
  render() {
    const { flashsets } = this.state;
    const { auth, profileUser } = this.props;
    return (
      <Layout title="flasher.ge">
        <div className="profilePage">
          <div className="header">
            <div className="container">
              <div className="user__data">
                <figure>
                  <img
                    src="https://gimg.quizlet.com/-_Uv01hFT798/AAAAAAAAAAI/AAAAAAAAAjM/-kDnfadKBtA/photo.jpg?sz=116"
                    alt=""
                  />
                </figure>
                <div className="content">
                  <div className="headline">
                    <div className="nickname">{profileUser.nickname}</div>
                    <div className="fullname">{profileUser.name}</div>
                  </div>
                  <div className="tabs">
                    <NextLink route="flashsetCreate">
                      <a className="tab">ახლის შექმნა</a>
                    </NextLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="body">
            <div className="container">
              <div className="row">
                <div className="col-md-7">
                  <div className="fs__g">
                    {/* <div className="fs__g__header">
                      <h6>In May 2019</h6> <span className="line" />
                    </div> */}

                    {flashsets.map(flash => (
                      <FlashItem
                        key={flash._id}
                        flash={flash}
                        redirectToFlashsetEdit={this.redirectToFlashsetEdit}
                        deleteFlashset={this.deleteFlashset}
                        auth={auth}
                      />
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

const FlashItem = props => {
  const [deleting, setDeleting] = useState(false);
  const { flash, auth } = props;
  return (
    <NextLink route={`/flashes/${flash._id}/${flash.slug}`} key={flash._id}>
      <div className="fs__item">
        <div className="counter">
          {flash.flashcards ? flash.flashcards.length : 0} Terms
        </div>
        <h2 className="h2">{flash.title}</h2>
        <span className="date">
          {moment(flash.createdAt).format("DD/MM/YYYY")}
        </span>
        {auth.user && auth.user._id == flash.creator && (
          <div className="actions">
            <button
              onClick={e => props.redirectToFlashsetEdit(e, flash._id)}
              className="btn btn-warning btn-sm mr-2"
            >
              <i className="far fa-edit" />
            </button>
            <button
              disabled={deleting}
              onClick={e => {
                e.preventDefault();
                if (!confirm("ნამდვილად გსურთ წაშლა")) return;
                setDeleting(true);
                props.deleteFlashset(e, flash._id);
              }}
              className="btn btn-danger btn-sm"
            >
              {deleting ? (
                <i className="fas fa-spinner fa-spin" />
              ) : (
                <i className="fa fa-trash" />
              )}
            </button>
          </div>
        )}
      </div>
    </NextLink>
  );
};

const mapStateToProps = state => ({
  isAuth: state.auth.isAuth,
  auth: state.auth
});
export default connect(mapStateToProps)(Porfile);
