import Layout from "../components/layout";

import FlashCardSlider from "../components/FlashCard/FlashCardSlider";
import TermList from "../components/Term/TermList";
import FlashModeContainer from "../components/SetType/FlashModeContainer";
import axios from "axios";
import UserFlashInfo from "../components/UserFlashInfo";
import { connect } from "react-redux";
import { set_flashset } from "../redux/flashcards/flashcardActions";
import { fetchFlashsetsUrl } from "../serverRoutes";
import FlashCard from "../components/FlashCard/FlashCard";
import classnames from "classnames";

class Flashcards extends React.Component {
  static async getInitialProps({ reduxStore, req, query }) {
    const flash_set = reduxStore.getState().flash.flash_set;
    if (!flash_set || (flash_set && flash_set.slug != query.slug)) {
      const url = fetchFlashsetsUrl({ id: query.id });
      let response;
      try {
        response = await axios({
          method: url.method,
          url: url.path
        });
        reduxStore.dispatch(
          set_flashset({
            flashcards: response.data.flashcards,
            flashset: response.data.flashset
          })
        );
      } catch (err) {
        console.log(err);
      }
    }
    return {};
  }

  render() {
    // const bottomOrLeft = ;
    // console.log(bottomOrLeft);
    return (
      <Layout title={this.props.flash_set.title + " - flasher.ge"}>
        <div className="container-fluid text-center">
          <h1 className="pl-1 flashset__title">{this.props.flash_set.title}</h1>

          <FlashModeContainer flash_set={this.props.flash_set} />
          <div className="row">
            {this.props.flashcards.map((flash, index) => (
              <div
                className={classnames(
                  "relative col-md-6 col-lg-4",
                  { "anime-top-to-bottom": index % 2 },
                  { "anime-fade-in-right": !index % 2 }
                )}
                style={{ marginTop: "20px" }}
              >
                <FlashCard flashCard={flash} />
              </div>
            ))}
          </div>
        </div>
        <div className="container mt-5">
          <UserFlashInfo />
        </div>
      </Layout>
    );
  }
}
// export default withRouter(Flashcards);

const mapStateToProps = state => ({
  flash_set: state.flash.flash_set,
  flashcards: state.flash.flashcards
});

export default connect(mapStateToProps)(Flashcards);
