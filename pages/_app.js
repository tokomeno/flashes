import App, { Container } from "next/app";
import React from "react";
import withReduxStore from "../redux/with-redux-store";
import { Provider } from "react-redux";
import { LOGIN_USER } from "../redux/auth/authTypes";

import NProgress from "nprogress";

import Router from "next/router";
import { getCookesFromReq } from "../helpers/utils";
import { initGA, logPageView } from "../services/ga";
import { hotjar } from "react-hotjar";

import axios from "axios";
import Cookies from "js-cookie";

import { hideMenu } from "../redux/ui/uiActions";
import { getOrCreateStore } from "../redux/with-redux-store";

Router.events.on("routeChangeStart", url => {
  NProgress.start();
});
Router.events.on("routeChangeComplete", () => {
  getOrCreateStore().dispatch(hideMenu());
  NProgress.done();
});
Router.events.on("routeChangeError", () => NProgress.done());

class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    if (!process.browser) {
      const currentTime = Date.now() / 1000;
      const expiresAt = getCookesFromReq(ctx.req, "expiresAt");
      if (expiresAt > currentTime) {
        let user = getCookesFromReq(ctx.req, "user");
        const token = getCookesFromReq(ctx.req, "token");
        if (user && token) {
          user = JSON.parse(user);
          ctx.reduxStore.dispatch({
            type: LOGIN_USER,
            payload: { user, token }
          });
        }
      }
    }
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    return { pageProps };
  }
  componentDidMount() {
    window.axios = axios;
    let token = Cookies.get("token");
    if (token) {
      axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    }
    if (process.env.NODE_ENV == "production") {
      if (window) {
        hotjar.initialize(1457473);
      }
      if (!window.GA_INITIALIZED) {
        initGA();
        window.GA_INITIALIZED = true;
      }
      logPageView();
    }
  }

  render() {
    const { Component, pageProps, reduxStore } = this.props;
    // console.log(reduxStore);
    return (
      <Container>
        <Provider store={reduxStore}>
          <Component {...pageProps} />
        </Provider>
      </Container>
    );
  }
}

export default withReduxStore(MyApp);
