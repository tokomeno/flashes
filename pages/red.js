import Layout from "../components/layout";
import Link from "next/link";
import { Link as NextLink } from "../routes";
import React from "react";
import { connect } from "react-redux";
import { startClock, serverRenderClock } from "../store";

class Index extends React.Component {
  static getInitialProps({ reduxStore, req }) {
    const isServer = !!req;
    // DISPATCH ACTIONS HERE ONLY WITH `reduxStore.dispatch`
    reduxStore.dispatch(serverRenderClock(isServer));

    return {};
  }

  componentDidMount() {
    console.log(this.props.testing);
    // DISPATCH ACTIONS HERE FROM `mapDispatchToProps`
    // TO TICK THE CLOCK
    this.timer = setInterval(() => this.props.startClock(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }
  render() {
    const flashcard = { slug: "flas-sdsad", id: 12 };
    const items = [
      {
        id: 1,
        title: "Bitcoin",
        text:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi cumque sequi debitis!",
        image: "https://cdn1.iconfinder.com/data/icons/learning-and-education-1/200/04-512.png"
      },
      {
        id: 2,
        title: "Steller",
        text:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi cumque sequi debitis!"
      },
      {
        id: 3,
        title: "Etherium",
        text:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi cumque sequi debitis!"
      }
    ];
    return (
      <Layout title="ისწავლე ფლეშქარდებით მარტივად - flasher.ge">
        <Link
          href={`/flashes?id=${flashcard.id}&slug=${flashcard.slug}`}
          as={`/flashes/${flashcard.id}/${flashcard.slug}`}
        >
          <a>open flashes</a>
        </Link>
        <br />
        <NextLink route={`/flashes/${flashcard.id}/${flashcard.slug}`}>
          <a>open flashes</a>
        </NextLink>
        {/*  */}
        <div className="home_page">
          <div className="bg" />
          <div className="bg2" />
          <div className="home__container">
            <div className="left">
              <h4>
                <b>Flashcards</b>
              </h4>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor voluptates beatae
              consequuntur dolorem, qui, pariatur libero illum odit quam, totam ipsum consectetur
              aliquid similique quo sed placeat accusantium corrupti. Rem.
            </div>
            <div className="right">
              <ul>
                {items.map(item => {
                  return (
                    <li key={item.id}>
                      <img src={item.image} alt={item.alt} />
                      <div className="content">
                        <h4>{item.title}</h4>
                        <p>{item.text}</p>
                      </div>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

// export default Home
const mapStateToProps = state => ({
  testing: state.testing
});
const mapDispatchToProps = { startClock };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index);
