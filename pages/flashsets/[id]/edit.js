import Layout from "../../../components/layout";

import { connect } from "react-redux";

import FlashsetList from "../../../components/CreateFlashset/list";
import axios from "axios";
import { fetchFlashsetsUrl } from "../../../serverRoutes";
import withAuth from "../../../components/hoc/withAuth";

// import "../../static/materiali_comps.css";

class FlashesPage extends React.Component {
  static async getInitialProps({ reduxStore, req, query }) {
    const url = fetchFlashsetsUrl({ id: query.id });
    let response;
    try {
      response = await axios({
        method: url.method,
        url: url.path
      });
    } catch (err) {
      console.log(err);
    }
    return {
      flashset: response.data.flashset,
      flashcards: response.data.flashcards
    };
  }

  render() {
    return (
      <Layout title={" - flasher.ge"}>
        <div className="container">
          <FlashsetList
            flashset={this.props.flashset}
            flashcards={this.props.flashcards}
          />
        </div>
      </Layout>
    );
  }
}

export default withAuth(FlashesPage);
