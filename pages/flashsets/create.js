import Layout from "../../components/layout";

import FlashsetList from "../../components/CreateFlashset/list";
import withAuth from "../../components/hoc/withAuth";

// import "../../static/materiali_comps.css";

class FlashesPage extends React.Component {
  // static async getInitialProps({ reduxStore, req, query }) {}

  render() {
    return (
      <Layout title={" - flasher.ge"}>
        <div className="container">
          <FlashsetList />
        </div>
      </Layout>
    );
  }
}

export default withAuth(FlashesPage);
