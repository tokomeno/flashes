import Layout from "../components/layout";
import React from "react";
import { withRouter } from "next/router";

import LoginForm from "../components/Auth/LoginForm";
import RegisterForm from "../components/Auth/RegisterForm";

class Register extends React.Component {
  static async getInitialProps(ctx) {
    if (ctx.reduxStore.getState().auth.isAuth) {
      if (process.browser) {
        Router.push("/");
      } else {
        ctx.res.redirect("/");
      }
    }
    return {};
  }
  constructor(props) {
    super(props);
    this.state = {
      showLogin: false
    };
  }
  toggleFormHandler = () => {
    this.setState(prevState => ({ showLogin: !this.state.showLogin }));
  };
  async componentDidMount() {}
  render() {
    return (
      <Layout title="ავტორიზაცია - flasher.ge">
        <div className="mt-3 login__container">
          {this.state.showLogin ? (
            <LoginForm toggleForm={this.toggleFormHandler} />
          ) : (
            <RegisterForm toggleForm={this.toggleFormHandler} />
          )}
        </div>
      </Layout>
    );
  }
}

export default withRouter(Register);
